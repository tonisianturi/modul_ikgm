# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Aplikasi IKGM',
    'version' : '1.1',
    'summary': 'Aplikasi Perusahaan IKGM',
    'sequence': 15,
    'description': """
Invoicing & Payments
====================
The specific and easy-to-use Invoicing system in Odoo allows you to keep track of your accounting, even when you are not an accountant. It provides an easy way to follow up on your vendors and customers.

You could use this simplified accounting in case you work with an (external) account to keep your books, and you still want to keep track of payments. This module also offers you an easy method of registering payments, without having to encode complete abstracts of account.
    # """,
    'category': 'Invoicing Management',
    'website': 'https://www.odoo.com/page/billing',
    'depends' : ['website',
    ],
    'data': [
        'security/ir.model.access.csv',
        'view/tabel_perusahaan_view.xml',
        'view/tabel_unit_usaha_view.xml',
        'view/tabel_grup_sandi_bi_view.xml',
        'view/tabel_personal_level_view.xml',
        'view/tabel_sandi_bi_view.xml',
        'view/tabel_bidang_usaha_view.xml',
        'view/tabel_penggunaan_pinjaman_view.xml',
        'view/tabel_provinsi_view.xml',
        'view/tabel_kabupaten_view.xml',
        'view/tabel_kecamatan_view.xml',
        'view/tabel_kelurahan_view.xml',
        'view/tabel_gelar_view.xml',
        'view/tabel_jenis_promo_view.xml',
        'view/tabel_pendidikan_view.xml',
        'view/tabel_alasan_tolak_view.xml',
        'view/tabel_divisi_view.xml',
        'view/tabel_departmen_view.xml',
        'view/tabel_barang_promo_view.xml',
        'view/tabel_alasan_batal_view.xml',
        'view/tabel_profesi_view.xml',
        'view/tabel_kelompok_nr_view.xml',
        'view/tabel_format_nr_view.xml',
        'view/tabel_group_nr_view.xml',
        'view/tabel_main_account_view.xml',
        'view/tabel_sifat_kredit_view.xml',
        'view/tabel_alasan_tidak_tertagih_view.xml',
        'view/tabel_pendapatan_tidak_kena_pajak_view.xml',
        'view/tabel_jabatan_view.xml',
        'view/tabel_golongan_debitur_view.xml',
        'view/tabel_golongan_kreditur_view.xml',
        'view/tabel_golongan_pemilik_view.xml',
        'view/tabel_golongan_penjamin_view.xml',
        'view/tabel_maskar_view.xml',
        'menu.xml',
    ],
    'demo': [
        #'demo/account_demo.xml',
    ],
    
    'installable': True,
    'application': True,
    'auto_install': False,
}
