from odoo import api,fields,models,_
from odoo.exceptions import Warning


class TabelPendapatanTidakKenaPajak(models.Model):
	_name 		='hr.ptkp'
	_rec_name 	='KDPTKP'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDPTKP		=fields.Char(string='Kode Tidak Kena Pajak',required=True)
	KET		 	=fields.Char(string='KET', required=True)
	PTKP		=fields.Integer(string='Pendapatan Tidak Kena Pajak')


	# onchange validasi tidak boleh double
	@api.onchange('KDPTKP')
	def onchange_KDPTKP(self):
		if self.KDPTKP:
			KDPTKP_exist=self.env['hr.ptkp'].sudo().search([
			('KDPTKP','=',self.KDPTKP)
			])
			if KDPTKP_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Tidak Kena Pajak sudah dipakai!'
				}
				return{'value':{'KDPTKP':''},'warning':warning}

# Validation save
	@api.model
	def create(self, vals):
		if vals.get('PTKP') < 1 :
			raise Warning ('Pendapatan Tidak Kena Pajak Harus Lebih Besar Dari 0!')
		return super(TabelPendapatanTidakKenaPajak, self).create(vals)

	# validation write
	@api.multi
	def write(self, vals):
		if vals.get('PTKP') < 1 :
			raise Warning ('Pendapatan Tidak Kena Pajak Harus Lebih Besar Dari 0!')
		return super(TabelPendapatanTidakKenaPajak, self).write(vals)
