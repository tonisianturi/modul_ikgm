from odoo import api,fields,models,_

class TabelKabupaten(models.Model):
	_name 		= 'um.kabupaten'
	_rec_name 	='KDKABUD'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDPROP	 	=fields.Many2one(comodel_name='um.provinsi',string='Kode Provinsi')
	KDKABUD		=fields.Char(string='Kode Kabupaten',required=True)
	NMKABUD	 	=fields.Char(string='Nama Kabupaten', required=True)
	KDGSBI		=fields.Many2one(comodel_name='um.grpsbi',string='Kode Grup Sandi BI')
	KDSBI		=fields.Many2one(comodel_name='um.sbi',string='Kode Sandi BI')


# onchange kode kabupaten
	@api.onchange('KDKABUD')
	def onchange_KDKABUD(self):
		if self.KDKABUD:
			KDKABUD_exist=self.env['um.kabupaten'].sudo().search([
				('KDKABUD','=',self.KDKABUD)
				])
			if KDKABUD_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Kabupaten sudah dipakai!'
				}
				return{'value':{'KDKABUD':''},'warning':warning}

#Onchange terisi otomatis
	@api.onchange('KDGSBI')
	def onchange_kode_grup_sandi_bi(self):
		self.KDSBI =False
		kodesandibi ={'KDSBI':[('KDGSBI','=',self.KDGSBI.id)]}
		return {'domain':kodesandibi}
	