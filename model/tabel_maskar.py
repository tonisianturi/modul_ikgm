from odoo import api,fields,models,_
from odoo.exceptions import Warning



class TabelMaskar(models.Model):
	_name ='hr.maskar'


	KDPRS				=fields.Char(string='Kode Perusahaan', required=True)
	KDUNITU				=fields.Char(string='Kode Unit Usaha', required=True)
	NIK					=fields.Char(string='NIK',required=True)
	NMKAR				=fields.Char(string='Nama Karyawan',required=True)
	TMPLAHIR			=fields.Char(string='Tempat Lahir')
	TGLLAHIR			=fields.Char(string='Tanggal Lahir')
	EKAR				=fields.Char(string='Email Karyawan')
	TGLIN				=fields.Date(string='Tanggal Masuk',required=True)
	JNSKEL				=fields.Selection([('P','Perempuan'),('L','Laki-Laki')],string='Jenis Kelamin',required=True)
	NOKTP				=fields.Char(string='Nomor KTP')
	NONPWP				=fields.Char(string='Nomor NPWP')
	NMGI				=fields.Char(string='NMGI')
	ALMKTP				=fields.Char(string='Alamat KTP')
	ALMRMH				=fields.Char(string='Alamar Rumah')
	KDDIV				=fields.Many2one(comodel_name='hr.divisi', string='Kode Devisi',required=True)
	KDDEPT				=fields.Many2one(comodel_name='hr.dept', string='Kode Departmen', required=True)
	KDLVL				=fields.Many2one(comodel_name='hr.p.level', string='Kode Level',required=True)
	KDPTKP				=fields.Many2one(comodel_name='hr.ptkp',string='Kode Pendapatan Tidak Kena Pajak', required=True)
	KDJAB				=fields.Many2one(comodel_name='hr.jabatan',string='Kode Jabatan', required=True)	
	AGAMA				=fields.Selection([('Islam','Islam'),('Protestan','Kristen Protestan'),('Katholik','Kristen Katholik'),
						('Hindu','Hindu'),('Budha','Budha'),('Konghuchu','Konghuchu')], string='Agama',required=True)
	KDPDDK				=fields.Many2one(comodel_name='hr.pddk', string='Kode Pendidikan',required=True)	
	JURUSAN				=fields.Char(string='Jurusan')
	STKAR				=fields.Char(string='STKAR')
	NOJAMSOS			=fields.Char(string='Nomor Jamsostek')
	CBGJ				=fields.Selection([('T','Transfer'),('C','Cash')],string='Cara Bayar Gaji')
	BPGJ				=fields.Char(string='Bank Pembayaran Gaji')
	NOREKB				=fields.Char(string='Nomor Rekening Bank')
	NMREKB				=fields.Char(string='Nama Rekening Bank')
	GAPOK				=fields.Float(string='Gaji Pokok')
	DUMUT				=fields.Selection([('Y','Ya'),('T','Tidak')], string='Dapat Uang Makan ?')
	CBUMUT				=fields.Selection([('H','Harian'),('M','Mingguan'),('B','Bulanan')],string='Cara Bayar Uang Makan')
	UMUT				=fields.Float(string='Uang Makan Uang Transportasi')
	TJAB				=fields.Float(string='Tunjangan Jabatan')
	TMED				=fields.Float(string='Tunjangan Medis')
	TFUNG				=fields.Float(string='Tunjangan Fungsional')
	TKOST				=fields.Float(string='Tunjangan Uang Kost')
	TTMP				=fields.Float(string='Tunjangan Tempat')
	THPP				=fields.Float(string='THPP')
	TGLOUT				=fields.Date(string='Tanggal Keluar')
	TGLCATAT			=fields.Date(string='Tanggal Catat')
	JAMCATAT			=fields.Char(string='Jam Catat')
	PASSWORD			=fields.Char(string='Password',required=True)


	 # Onchange Tidak Boleh Double NIK
	@api.onchange('NIK')
	def onchange_nik(self):
		if self.NIK:
			nik_exist = self.env['hr.maskar'].sudo().search([
			('NIK','=',self.NIK)
			])
			if nik_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'NIK sudah digunakan!!!'
				}
				return{'value':{'NIK':''},'warning':warning}

	# Onchange Tidak Boleh Double KTP
	@api.onchange('NOKTP')
	def onchange_noktp(self):
		if self.NOKTP and self.NOKTP:
			noktp_exist = self.env['hr.maskar'].sudo().search([
			('NOKTP','=',self.NOKTP)])
			if ksb_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'No KTP sudah digunakan!!!'
				}
				return{'value':{'NOKTP':''},'warning':warning}

	# Onchange Tidak Boleh Double NPWP
	@api.onchange('NPWP')
	def onchange_npwp(self):
		if self.NPWP and self.NPWP:
			npwp_exist = self.env['hr.maskar'].sudo().search([
			('NPWP','=',self.NPWP)])
			if npwp_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'No NPWP sudah digunakan!!!'
				}
				return{'value':{'NPWP':''},'warning':warning}

	# Onchange Tidak Boleh Double NO. Jamsos
	@api.onchange('NOJAMSOS')
	def onchange_nojamsos(self):
		if self.NOJAMSOS and self.NOJAMSOS:
			nojamsos_exist = self.env['hr.maskar'].sudo().search([
			('NOJAMSOS','=',self.NOJAMSOS)])
			if nojamsos_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'No Jamsos sudah digunakan!!!'
				}
				return{'value':{'NOJAMSOS':''},'warning':warning}

	# Validation Save
	@api.model
	def create(self,cek):
			if ((cek.get('DUMUT') == 'Y') and (cek.get('UMUT') == 0.00)): 
				raise Warning ('Tidak Boleh Nol!!!')
			return super(TabelMaskar, self).create(cek)

	@api.multi
	def write(self,cek):
			if ((cek.get('DUMUT') == 'Y') and (cek.get('UMUT') == 0.00)):
				raise Warning ('Tidak Boleh Nol!!!')
			return super(TabelMaskar, self).write(cek)


	# # # Validation Save CBGJ
	# @api.model
	# def create(self,cek):
	# 		if (cek.get('CBGJ') == 'T' and cek.get('BPGJ') ==" "):
	# 			raise Warning ('Tidak Boleh Kosong!!!')
	# 		return super(TabelMaskar, self).create(cek)


