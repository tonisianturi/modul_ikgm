from odoo import api,fields,models,_



class TabelDepertmen(models.Model):
	_name ='hr.dept'
	_rec_name ='KDDEPT'


	KDPRS		=fields.Char(string='Kode Perusahaan', required=True)
	KDDEPT	 	=fields.Char(string='Kode Departmen', required=True)
	NMDEPT 		=fields.Char(string='Nama Departmen', required=True)
	KDDIV		=fields.Many2one(comodel_name='hr.divisi',string='Kode Devisi')

	 # Onchange Tidak Boleh Double
	@api.onchange('KDDEPT')
	def onchange_kgsb(self):
		if self.KDDEPT:
			kode_department_exist = self.env['hr.dept'].sudo().search([
			('KDDEPT','=',self.KDDEPT)
			])
			if kode_department_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Grup Departmen sudah digunakan!!!'
				}
				return{'value':{'KDDEPT':''},'warning':warning}

