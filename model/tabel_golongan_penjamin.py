from odoo import api,fields,models,_



class TabelGolonganPenjamin(models.Model):
	_name ='ks.golpjamin'


	KDPRS	=fields.Char(string='Kode Perusahaan', required=True)
	KDGOLP	=fields.Char(string='Kode Golongan Penjamin', required=True)
	GOLPJ 	=fields.Char(string='Golongan Penjamin', required=True)
	KDGSBI	=fields.Many2one(comodel_name="um.grpsbi",string='Kode Grup Sandi BI')
	KDSBI 	=fields.Many2one(comodel_name="um.sbi",string='Kode Sandi BI')


	# Tidak Boleh Double
	@api.onchange('KDGOLP')
	def onchange_kgp(self):
		if self.KDGOLP:
			kgp_exist = self.env['ks.golpjamin'].sudo().search([
			('KDGOLP','=',self.KDGOLP)
			])
			if kgp_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Golongan Penjamin sudah digunakan!!!'
				}
				return{'value':{'KDGOLP':''},'warning':warning}


	#Onchange terisi otomatis
	@api.onchange('KDGSBI')
	def onchange_kode_grup_sandi_bi(self):
		self.KDSBI =False
		kodesandibi ={'KDSBI':[('KDGSBI','=',self.KDGSBI.id)]}
		return {'domain':kodesandibi}
		
	# Tidak Boleh Diedit Kode Penggunaan Pinjaman	