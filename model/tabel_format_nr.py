from odoo import api,fields,models,_
from odoo.exceptions import Warning

class TabelFormatNR(models.Model):
	_name 		= 'gl.fornr'
	_rec_name	= 'KDFORNR'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDCAB	 	=fields.Many2one(comodel_name='sys.unitush',string='Kode Cabang')
	BI	 		=fields.Selection([('b','B'),('i','I')], string='BI')
	NOURUT		=fields.Char(string='No Urut',required=True)
	KDFORNR		=fields.Char(string='Kode Format NR',required=True)
	FORNR		=fields.Char(string='Format NR',required=True)
	KDKELNR		=fields.Many2one(comodel_name='gl.kelnr',string='Kode Kelompok NR')
	SN			=fields.Selection([('Debet','D'),('Credit','C')],string='Saldo Normal')
	KURANG		=fields.Selection([('y','Y'),('t','T')],string='Kurang')
	CETAK		=fields.Selection([('y','Y'),('t','T')],string='Cetak')
	

	# onchange kode Format NR
	@api.onchange('KDFORNR')
	def onchange_KDFORNR(self):
		if self.KDFORNR:
			KDFORNR_exist=self.env['gl.fornr'].sudo().search([
			('KDFORNR','=',self.KDFORNR)
			])
			if KDFORNR_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Format NR sudah dipakai!'
				}
				return{'value':{'KDFORNR':''},'warning':warning}
