from odoo import api,fields,models,_



class BidangUsaha(models.Model):
	_name ='ks.bidush'


	KDPRS		=fields.Char(string='Kode Perusahaan', required=True)
	KDBIDU 		=fields.Char(string='Kode Bidang Usaha', required=True)
	BIDUSH 		=fields.Char(string='Bidang Usaha', required=True)
	KDGSBI		=fields.Many2one(comodel_name='um.grpsbi',string='Kode Grup Sandi BI')
	KDSBI		=fields.Many2one(comodel_name='um.sbi',string='Kode Sandi BI')


	# Tidak Boleh Double
	@api.onchange('KDBIDU')
	def onchange_kbu(self):
		if self.KDBIDU:
			kbu_exist = self.env['ks.bidush'].sudo().search([
			('KDBIDU','=',self.KDBIDU)
			])
			if kbu_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode bidang usaha sudah digunakan!!!'
				}
				return{'value':{'KDBIDU':''},'warning':warning}


	#Onchange terisi otomatis
	@api.onchange('KDGSBI')
	def onchange_kode_grup_sandi_bi(self):
		self.KDSBI =False
		kodesandibi ={'KDSBI':[('KDGSBI','=',self.KDGSBI.id)]}
		return {'domain':kodesandibi}
