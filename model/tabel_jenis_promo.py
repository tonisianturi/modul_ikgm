from odoo import api,fields,models,_

class TabelJenisPromo(models.Model):
	_name 		='ks.jpromo'
	_rec_name	='KDJPROMO'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDJPROMO	=fields.Char(string='Kode Jenis Promo', required = True)
	KET		 	=fields.Char(string='Keterangan', required=True)
	PAKAI		=fields.Selection([('y','Ya'),('t','Tidak')], string='Pakai')
	

	# onchange kode jenis promo
	@api.onchange('KDJPROMO')
	def onchange_KDJPROMO(self):
		if self.KDJPROMO:
			KDJPROMO_exist=self.env['ks.jpromo'].sudo().search([
			('KDJPROMO','=',self.KDJPROMO)
			])
			if KDJPROMO_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Jenis Promo sudah dipakai!'
				}
				return{'value':{'KDJPROMO':''},'warning':warning}