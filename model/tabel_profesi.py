from odoo import api,fields,models,_

class TabelProfesi(models.Model):
	_name 		= 'ks.prof'
	_rec_name 	='KDPROF'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDPROF	 	=fields.Char(string='Kode Profesi', required=True)
	NMPROF		=fields.Char(string='Nama Profesi',required=True)
	JENIS		=fields.Selection([('karyawan','Karyawan'),('wiraswasta/pengusaha','Wiraswasta/Pengusaha')], string='Jenis')
	KDGSBI		=fields.Many2one(comodel_name='um.grpsbi',string='Kode Grup Sandi BI')
	KDSBI		=fields.Many2one(comodel_name='um.sbi',string='Kode Sandi BI')


# onchange kode profesi
	@api.onchange('KDPROF')
	def onchange_KDPROF(self):
		if self.KDPROF:
			KDPROF_exist=self.env['ks.prof'].sudo().search([
				('KDPROF','=',self.KDPROF)
				])
			if KDPROF_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Profesi sudah dipakai!'
				}
				return{'value':{'KDPROF':''},'warning':warning}

#Onchange terisi otomatis
	@api.onchange('KDGSBI')
	def onchange_kode_grup_sandi_bi(self):
		self.KDSBI =False
		kodesandibi ={'KDSBI':[('KDGSBI','=',self.KDGSBI.id)]}
		return {'domain':kodesandibi}
	