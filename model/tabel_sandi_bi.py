from odoo import api,fields,models,_



class SandiBI(models.Model):
	_name ='um.sbi'
	_rec_name='KDSBI'


	KDPRS		=fields.Char(string='Kode Perusahaan', required=True)
	KDGSBI  	=fields.Many2one(comodel_name='um.grpsbi',string='Kode Grup Sandi BI', required=True)
	KDSBI 		=fields.Char(string='Kode Sandi BI', required=True)
	KET 		=fields.Char(string='Keterangan', required=True)

	# Onchange Tidak Boleh Double
	@api.onchange('KDSBI', 'KDGSBI')
	def onchange_ksb(self):
		if self.KDSBI and self.KDGSBI:
			ksb_exist = self.env['um.sbi'].sudo().search([
			('KDSBI','=',self.KDSBI),('KDGSBI','=',self.KDGSBI.id)
			])
			if ksb_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Sandi BI sudah digunakan!!!'
				}
				return{'value':{'KDSBI':'','KDGSBI':''},'warning':warning}


	