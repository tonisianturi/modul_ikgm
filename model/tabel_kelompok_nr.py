from odoo import api,fields,models,_
from odoo.exceptions import Warning

class TabelKelompokNR(models.Model):
	_name 		='gl.kelnr'
	_rec_name	='KDKELNR'
	

	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDCAB	 	=fields.Many2one(comodel_name='sys.unitush',string='Kode Cabang')
	BI	 		=fields.Selection([('b','B'),('i','I')], string='BI')
	NOURUT	 	=fields.Integer(string='No Urut', required = True)
	KDKELNR	 	=fields.Char(string='Kode Kelompok NR', required = True)
	KELNR	 	=fields.Char(string='Kelompok NR', required = True)


# onchange kode kelompok NR
	@api.onchange('KDKELNR')
	def onchange_KDKELNR(self):
		if self.KDKELNR:
			KDKELNR_exist=self.env['gl.kelnr'].sudo().search([
			('KDKELNR','=',self.KDKELNR)
			])
			if KDKELNR_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Kelompok NR sudah dipakai!'
				}
				return{'value':{'KDKELNR':''},'warning':warning}

	
