from odoo import api,fields,models,_

class TabelKecamatan(models.Model):
	_name 		= 'um.kecamatan'
	_rec_name	='KDCAMAT'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDPROP	 	=fields.Many2one(comodel_name='um.provinsi',string='Kode Provinsi')
	KDKABUD	 	=fields.Many2one(comodel_name='um.kabupaten',string='Kode Kabupaten')
	KDCAMAT	 	=fields.Char(string='Kode Kecamatan', required=True)
	NMCAMAT	 	=fields.Char(string='Nama Kecamatan', required=True)

	#ONCHANGE DOMAIN
	@api.onchange('KDPROP')
	def onchange_kecamatan(self):
		self.KDKABUD = False
		#print("=====================",self.nama_provinsi)
		domain_kabupaten = {'KDKABUD': [('KDPROP', '=', self.KDPROP.id)]}
		return {'domain':domain_kabupaten}
	 
	 # onchange kode kecamatan
	@api.onchange('KDCAMAT','KDKABUD')
	def onchange_KDCAMAT(self):
		if self.KDCAMAT and self.KDKABUD:
			KDCAMAT_exist=self.env['um.kecamatan'].sudo().search([
				('KDCAMAT','=',self.KDCAMAT),('KDKABUD','=',self.KDKABUD.id)
				])
			if KDCAMAT_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Kecamatan sudah dipakai!'
				}
				return{'value':{'KDCAMAT':'','KDKABUD':''},'warning':warning}
