from odoo import api,fields,models,_
from odoo.exceptions import Warning 

class TabelCabang(models.Model):
	_name 		= 'sys.unitush'
	_rec_name	='KDCAB'
	#_rec_name='nama'

	KDPRS 			= fields.Many2one(comodel_name='sys.perusahaan',string='Kode Perusahaan', required = True)
	KDCAB			= fields.Char(string='Kode Cabang', required = True, index = True)
	NMPRS			= fields.Char(string='Nama Cabang', required = True)
	ALAMAT			= fields.Text(string='Alamat', required = True)
	KOTA 			= fields.Char(string='Kota', required = True)
	TELP			= fields.Char(string='Telepon', required = True)
	PIMPINAN		= fields.Char(string='Pimpinan', required = True)
	TGLKUASA 		= fields.Date(string='Tanggal Kuasa')
	TTDDI 			= fields.Char(string='TTDDI')
	BANKTRF			= fields.Char(string='Bank Transfer')
	NOREK			= fields.Char(string='Nomor Rekening')
	JNSPRS			= fields.Selection([('Kantor Pusat', 'Kantor Pusat'), ('Cabang','Cabang'), ('Posko', 'Posko'), ('Pos Sales','Pos Sales'), ('Kantor Kas','Kantor Kas')], string='Jenis Perusahaan')
	CABASAL			= fields.Char(string='Cabang Asal')
	SING			= fields.Char(string='Singkatan')
	PBUKU			= fields.Selection([('Ya', 'Y'), ('Tidak','T')], string='Pembukuan')

	# Onchange Tidak Boleh Double
	@api.onchange('KDCAB', 'KDPRS')
	def onchange_kdcab(self):
		if self.KDCAB and self.KDPRS:
			kdcab_exist = self.env['sys.unitush'].sudo().search([
			('KDCAB','=',self.KDCAB),('KDPRS','=',self.KDPRS.id)
			])
			if kdcab_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Cabang sudah digunakan!!!'
				}
				return{'value':{'KDCAB':'','KDPRS':''},'warning':warning}
