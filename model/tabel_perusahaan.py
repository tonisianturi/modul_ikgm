from odoo import api,fields,models,_
from odoo.exceptions import Warning 

class TabelPerusahaan(models.Model):
	_name = 'sys.perusahaan'
	_rec_name='KDPRS'

	KDPRS				= fields.Char(string='Kode Perusahaan', required = True, index = True)
	NMPRS				= fields.Char(string='Nama Perusahaan', required = True, index = True)
	ALAMAT				= fields.Text(string='Alamat', required = True)
	KOTA				= fields.Char(string='Kota', required = True)
	TELP				= fields.Char(string='Telepon', required = True)
	PIMPINAN			= fields.Char(string='Pimpinan', required = True)
	SISTEM				= fields.Selection([('BPR', 'BPR'), ('Koperasi','Koperasi')], string='sistem')

	# @api.onchange('KDPRS', 'NMPRS', 'ALAMAT', 'KOTA', 'TELP', 'PIMPINAN','SISTEM')
	# def set_caps(self):
	# 	val = str(self.KDPRS)
	# 	self.KDPRS = val.upper()

	#Validasi untuk KDPRS tidak Boleh Double
	@api.onchange('KDPRS')
	def onchange_kdprs(self):
		if self.KDPRS:
			kdprs_exist = self.env['sys.perusahaan'].sudo().search([
			('KDPRS','=',self.KDPRS)
			])
			if kdprs_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Perusahaan sudah digunakan!!!'
				}
				return{'value':{'KDPRS':''},'warning':warning}

	