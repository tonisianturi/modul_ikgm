from odoo import api,fields,models,_
from odoo.exceptions import Warning

class TabelGroupNR(models.Model):
	_name 		= 'gl.grpnr'
	_rec_name	= 'KDGRPNR'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDCAB	 	=fields.Many2one(comodel_name='sys.unitush',string='Kode Cabang')
	BI	 		=fields.Selection([('b','B'),('i','I')], string='BI')
	NOURUT		=fields.Char(string='No Urut',required=True)
	KDGRPNR		=fields.Char(string='Kode Group NR',required=True)
	GRPNR		=fields.Char(string='Group NR',required=True)
	KDKELNR		=fields.Many2one(comodel_name='gl.kelnr',string='Kode Kelompok NR')
	KDFORNR		=fields.Many2one(comodel_name='gl.fornr',string='Kode Format NR')
	CETAK		=fields.Selection([('y','Y'),('t','T')],string='Cetak')
	

	# onchange kode group NR
	@api.onchange('KDGRPNR')
	def onchange_KDGRPNR(self):
		if self.KDGRPNR:
			KDGRPNR_exist=self.env['gl.grpnr'].sudo().search([
			('KDGRPNR','=',self.KDGRPNR)
			])
			if KDGRPNR_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Group NR sudah dipakai!'
				}
				return{'value':{'KDGRPNR':''},'warning':warning}
