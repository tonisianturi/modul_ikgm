from odoo import api,fields,models,_

class TabelAlasanTidakTertagih(models.Model):
	_name 		= 'ks.alasantt'
	_rec_name	='KDATT'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDATT		=fields.Char(string='Kode Alasan Tidak Tertagih', required = True)
	KET		 	=fields.Char(string='KET', required = True)
	BOBOT		=fields.Selection([('0','0'),('1','1'),('2','2'),('3','3'),('4','4'),('5','5'),('6','6'),('7','7'),('8','8'),('9','9')], string='BOBOT')
	

# onchange kode provinsi
	@api.onchange('KDATT')
	def onchange_KDATT(self):
		if self.KDATT:
			KDATT_exist=self.env['ks.alasantt'].sudo().search([
				('KDATT','=',self.KDATT)
				])
			if KDATT_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Alasan Tidak Tertagih sudah dipakai!'
				}
				return{'value':{'KDATT':''},'warning':warning}

