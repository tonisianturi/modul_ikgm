from odoo import api,fields,models,_



class TabelGolonganKreditur(models.Model):
	_name ='ks.golkr'


	KDPRS	=fields.Char(string='Kode Perusahaan', required=True)
	KDGKR	=fields.Char(string='Kode Golongan Kreditur', required=True)
	GOLKR 	=fields.Char(string='Golongan Kreditur', required=True)
	KDGSBI	=fields.Many2one(comodel_name="um.grpsbi",string='Kode Grup Sandi BI')
	KDSBI 	=fields.Many2one(comodel_name="um.sbi",string='Kode Sandi BI')


	# Tidak Boleh Double
	@api.onchange('KDGKR')
	def onchange_kgk(self):
		if self.KDGKR:
			kgk_exist = self.env['ks.golkr'].sudo().search([
			('KDGKR','=',self.KDGKR)
			])
			if kgk_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Golongan Kreditur sudah digunakan!!!'
				}
				return{'value':{'KDGKR':''},'warning':warning}


	#Onchange terisi otomatis
	@api.onchange('KDGSBI')
	def onchange_kode_grup_sandi_bi(self):
		self.KDSBI =False
		kodesandibi ={'KDSBI':[('KDGSBI','=',self.KDGSBI.id)]}
		return {'domain':kodesandibi}
		
	# Tidak Boleh Diedit Kode Penggunaan Pinjaman	