from odoo import api,fields,models,_

class TabelPersonalLevel(models.Model):
	_name 		='hr.p.level'
	_rec_name	='KDLVL'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDLVL		=fields.Char(string='Kode Personal Level', required = True)
	KET		 	=fields.Char(string='KET', required = True)


# onchange kode PERSONAL level
	@api.onchange('KDLVL')
	def onchange_KDPLEVEL(self):
		if self.KDLVL:
			KDPLEVEL_exist=self.env['hr.p.level'].sudo().search([
				('KDLVL','=',self.KDLVL)
				])
			if KDPLEVEL_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Personal Level sudah dipakai!'
				}
				return{'value':{'KDLVL':''},'warning':warning}

