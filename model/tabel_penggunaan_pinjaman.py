from odoo import api,fields,models,_



class TabelPenggunaanPinjaman(models.Model):
	_name ='ks.pgunap'


	KDPRS		=fields.Char(string='Kode Perusahaan', required=True)
	KDPGUNAJ 	=fields.Char(string='Kode Penggunaan Pinjaman', required=True)
	PDGU 		=fields.Char(string='Penggunaan Pinjaman', required=True)
	KDGSBI		=fields.Many2one(comodel_name="um.grpsbi",string='Kode Grup Sandi BI')
	KDSBI 		=fields.Many2one(comodel_name="um.sbi",string='Kode Sandi BI')


	# Tidak Boleh Double
	@api.onchange('KDPGUNAJ')
	def onchange_kpp(self):
		if self.KDPGUNAJ:
			kpp_exist = self.env['ks.pgunap'].sudo().search([
			('KDPGUNAJ','=',self.KDPGUNAJ)
			])
			if kpp_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Penggunaan Pinjaman sudah digunakan!!!'
				}
				return{'value':{'KDPGUNAJ':''},'warning':warning}


	#Onchange terisi otomatis
	@api.onchange('KDGSBI')
	def onchange_kode_grup_sandi_bi(self):
		self.KDSBI =False
		kodesandibi ={'KDSBI':[('KDGSBI','=',self.KDGSBI.id)]}
		return {'domain':kodesandibi}
	