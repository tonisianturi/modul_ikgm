from odoo import api,fields,models,_



class TabelDevivi(models.Model):
	_name ='hr.divisi'
	_rec_name ='KDDIV'


	KDPRS		=fields.Char(string='Kode Perusahaan', required=True)
	KDDIV	 	=fields.Char(string='Kode Divisi', required=True)
	NMDIV 		=fields.Char(string='Nama Divisi', required=True)

	# Onchange Tidak Boleh Double
	@api.onchange('KDDIV')
	def onchange_kode_divisi(self):
		if self.KDDIV:
			kode_divisi_exist = self.env['hr.divisi'].sudo().search([
			('KDDIV','=',self.KDDIV)
			])
			if kode_divisi_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Divisi sudah digunakan!!!'
				}
				return{'value':{'KDDIV':''},'warning':warning}
