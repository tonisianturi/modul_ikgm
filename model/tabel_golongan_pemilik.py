from odoo import api,fields,models,_



class TabelGolonganPemilik(models.Model):
	_name ='ks.golpmlk'


	KDPRS	=fields.Char(string='Kode Perusahaan', required=True)
	KDGOLP	=fields.Char(string='Kode Golongan Pemilik', required=True)
	GOLPMLK =fields.Char(string='Golongan Pemilik', required=True)
	KDGSBI	=fields.Many2one(comodel_name="um.grpsbi",string='Kode Grup Sandi BI')
	KDSBI 	=fields.Many2one(comodel_name="um.sbi",string='Kode Sandi BI')


	# Tidak Boleh Double
	@api.onchange('KDGOLP')
	def onchange_kgpemilik(self):
		if self.KDGOLP:
			kgpemilik_exist = self.env['ks.golpmlk'].sudo().search([
			('KDGOLP','=',self.KDGOLP)
			])
			if kgpemilik_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Golongan Pemilik sudah digunakan!!!'
				}
				return{'value':{'KDGOLP':''},'warning':warning}


	#Onchange terisi otomatis
	@api.onchange('KDGSBI')
	def onchange_kode_grup_sandi_bi(self):
		self.KDSBI =False
		kodesandibi ={'KDSBI':[('KDGSBI','=',self.KDGSBI.id)]}
		return {'domain':kodesandibi}
		
	# Tidak Boleh Diedit Kode Penggunaan Pinjaman	