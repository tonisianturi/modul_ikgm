from odoo import api,fields,models,_
from odoo.exceptions import Warning


class TabelJabatan(models.Model):
	_name ='hr.jabatan'
	_rec_name ='KDJAB'



	KDPRS		=fields.Char(string='Kode Perusahaan', required=True)
	JNSPIC		=fields.Selection([('E','Eksternal'),('K','Karyawan')], string='Jenis PIC', required=True)
	KDJAB 		=fields.Char(string='Kode Jabatan', required=True)
	NMJAB 		=fields.Char(string='Nama Jabatan', required=True)
	KDDEPT		=fields.Many2one(comodel_name='hr.dept', string='Kode Departmen')
	DUMUT		=fields.Selection([('Y','Ya'),('T','Tidak')], string="Dapat Uang Makan Uang Transport", required=True)
	BUMUT 		=fields.Selection([('H','Harian'),('M','Mingguan'),('B','Bulanan')], string="Cara Bayar Uang Makan Uang Transport")
	UMUT 		=fields.Float(string='Uang Makan Uang Transport')
	TUNJJAB 	=fields.Float(string='Tunjangan Jabatan')
	TUNJMED		=fields.Float(string='Tunjangan Medis')
	TUNJFUNG 	=fields.Float(string='Tunjangan Fungsional')
	TUNJHP		=fields.Float(string='Tunjangan HP')

	# Onchange Tidak Boleh Double
	@api.onchange('KDJAB')
	def onchange_jabatan(self):
		if self.KDJAB:
			kode_jabatan_exist = self.env['hr.jabatan'].sudo().search([
			('KDJAB','=',self.KDJAB)
			])
			if kode_jabatan_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Jabatan sudah digunakan!!!'
				}
				return{'value':{'KDJAB':''},'warning':warning}

	#Validation Save
	@api.model
	def create(self,cek):
		if(cek.get('DUMUT') == 'Y') and (cek.get('UMUT') == 0.00): 
			raise Warning ('Nilai Uang Makan Uang Transport Tidak Boleh Kosong!!!')
		return super(TabelJabatan, self).create(cek)

	# @api.model
	# def create_data(self,cek):
	# 		if ((cek.get('dumut') != 'H'|'M'|'B') and (cek.get('umut') == 0.00)): 
	# 			raise Warning ('Tidak Boleh Kosong!!!')
	# 				# if((cek.get('umut')==0.00) and (cek.get('bumut') != 'H','M','B')):
	# 				# 	raise Warning ('Tidak Boleh Kosong!!!')
	# 		return super(TabelJabatan, self).create(cek)

	@api.multi
	def write(self,cek):
		# self.BUMUT = False
		if(cek.get('DUMUT') == 'Y') and (cek.get('UMUT') == 0.00): 
			raise Warning ('Nilai Uang Makan Uang Transport Tidak Boleh Kosong!!!')
		return super(TabelJabatan, self).write(cek)

	# @api.multi
	# def write(self,cek):
	# 		if ((cek.get('DUMUT') == 'Y') and ((cek.get('UMUT') == 0.00)) and ((cek.get('BUMUT') != ('H'or'M'or'B')))): 
	# 			raise Warning ('Tidak Boleh Nol!!!')
	# 		return super(TabelJabatan, self).write(cek)

