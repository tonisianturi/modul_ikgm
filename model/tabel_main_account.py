from odoo import api,fields,models,_
from odoo.exceptions import Warning

class TabelMainAcc(models.Model):
	_name 		= 'gl.mainacc'
	_rec_name	= 'MAINACC'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDCAB	 	=fields.Many2one(comodel_name='sys.unitush',string='Kode Cabang')
	BI	 		=fields.Selection([('b','B'),('i','I')], string='BI')
	MAINACC		=fields.Char(string='Main Acc',required=True)
	NMAINACC	=fields.Char(string='Nama Main Acc',required=True)
	KDGRPNR		=fields.Many2one(comodel_name='gl.grpnr',string='Kode Group NR')
	CETAK		=fields.Selection([('y','Y'),('t','T')],string='Cetak')
	

	# onchange mainacc
	@api.onchange('MAINACC')
	def onchange_MAINACC(self):
		if self.MAINACC:
			MAINACC_exist=self.env['gl.mainacc'].sudo().search([
			('MAINACC','=',self.MAINACC)
			])
			if MAINACC_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Main Acc sudah dipakai!'
				}
				return{'value':{'MAINACC':''},'warning':warning}
