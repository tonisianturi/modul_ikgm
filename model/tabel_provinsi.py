from odoo import api,fields,models,_

class TabelProvinsi(models.Model):
	_name 		= 'um.provinsi'
	_rec_name	='KDPROP'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDPROP	 	=fields.Char(string='Kode Provinsi', required = True)
	NMPROP	 	=fields.Char(string='Nama Provinsi', required = True)


# onchange kode provinsi
	@api.onchange('KDPROP')
	def onchange_KDPROP(self):
		if self.KDPROP:
			KDPROP_exist=self.env['um.provinsi'].sudo().search([
				('KDPROP','=',self.KDPROP)
				])
			if KDPROP_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Provinsi sudah dipakai!'
				}
				return{'value':{'KDPROP':''},'warning':warning}

	
