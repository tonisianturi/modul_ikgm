from odoo import api,fields,models,_

class TabelSifatKredit(models.Model):
	_name 		= 'ks.sifatk'
	_rec_name 	='KDSKR'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDSKR		=fields.Char(string='Kode Sifat Kredit', required=True)
	SIFATKR		=fields.Char(string='Sifat Kredit',required=True)
	KDGSBI		=fields.Many2one(comodel_name='um.grpsbi',string='Kode Grup Sandi BI')
	KDSBI		=fields.Many2one(comodel_name='um.sbi',string='Kode Sandi BI')


# onchange kode sifat kredit
	@api.onchange('KDSKR')
	def onchange_KDSKR(self):
		if self.KDSKR:
			KDSKR_exist=self.env['ks.sifatk'].sudo().search([
				('KDSKR','=',self.KDSKR)
				])
			if KDSKR_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Sifat Kredit sudah dipakai!'
				}
				return{'value':{'KDSKR':''},'warning':warning}

#Onchange terisi otomatis
	@api.onchange('KDGSBI')
	def onchange_kode_grup_sandi_bi(self):
		self.KDSBI =False
		kodesandibi ={'KDSBI':[('KDGSBI','=',self.KDGSBI.id)]}
		return {'domain':kodesandibi}
	