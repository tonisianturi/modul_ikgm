from odoo import api,fields,models,_

class TabelAlasanBatal(models.Model):
	_name 		='um.abatal'
	_rec_name	='KDABTL'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	JENIS		=fields.Selection([('sales','Sales'),('aplikasi','Aplikasi'), ('cif','CIF')], string='JENIS', default='cif')
	KDABTL		=fields.Char(string='Kode Alasan Batal', required=True)
	KET			=fields.Char(string='KET', required=True)


	# onchange kode alasan batal
	@api.onchange('KDABTL')
	def onchange_KDABTL(self):
		if self.KDABTL:
			KDABTL_exist=self.env['um.abatal'].sudo().search([
			('KDABTL','=',self.KDABTL)
			])
			if KDABTL_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Alasan Batal sudah dipakai!'
				}
				return{'value':{'KDABTL':''},'warning':warning}



	# @api.onchange('KDPRS')
	# def set_caps(self):
	# 	# self.KDPRS
	# 	val = str(self.KDPRS)
	# 	self.KDPRS = val.upper()