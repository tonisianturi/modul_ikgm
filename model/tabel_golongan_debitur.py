from odoo import api,fields,models,_



class TabelGolonganDebitur(models.Model):
	_name ='ks.goldeb'


	KDPRS		=fields.Char(string='Kode Perusahaan', required=True)
	KDGDEB	 	=fields.Char(string='Kode Golongan Debitur', required=True)
	GOLDEB 		=fields.Char(string='Golongan Debitur', required=True)
	KDGSBI		=fields.Many2one(comodel_name="um.grpsbi",string='Kode Grup Sandi BI')
	KDSBI 		=fields.Many2one(comodel_name="um.sbi",string='Kode Sandi BI')


	# Tidak Boleh Double
	@api.onchange('KDGDEB')
	def onchange_kgd(self):
		if self.KDGDEB:
			kgd_exist = self.env['ks.goldeb'].sudo().search([
			('KDGDEB','=',self.KDGDEB)
			])
			if kgd_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Golongan Debitur sudah digunakan!!!'
				}
				return{'value':{'KDGDEB':''},'warning':warning}


	#Onchange terisi otomatis
	@api.onchange('KDGSBI')
	def onchange_kode_grup_sandi_bi(self):
		self.KDSBI =False
		kodesandibi ={'KDSBI':[('KDGSBI','=',self.KDGSBI.id)]}
		return {'domain':kodesandibi}
		
	# Tidak Boleh Diedit Kode Penggunaan Pinjaman	