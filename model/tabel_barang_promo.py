
from odoo import api,fields,models,_

class TabelBarangPromo(models.Model):
	_name 		='um.brgpromo'
	_rec_name	='KDBPROMO'



	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDBRG		=fields.Char(string='Kode Barang', required = True)
	KDBPROMO	=fields.Char(string='Kode Barang Promo', required=True)
	QTY			=fields.Integer(string='QTY', required=True)
	HARGA		=fields.Float(string='HARGA', required=True)



	# onchange kode barang
	@api.onchange('KDBRG')
	def onchange_KDBRG(self):
		if self.KDBRG:
			KDBRG_exist=self.env['um.brgpromo'].sudo().search([
				('KDBRG','=',self.KDBRG)
				])
			if KDBRG_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Barang sudah dipakai!'
				}
				return{'value':{'KDBRG':''},'warning':warning}


	# onchange kode barang promo
	@api.onchange('KDBPROMO')
	def onchange_KDBPROMO(self):
		if self.KDBPROMO:
			KDBPROMO_exist=self.env['um.brgpromo'].sudo().search([
				('KDBPROMO','=',self.KDBPROMO)
				])
			if KDBPROMO_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Barang Promo sudah dipakai!'
				}
				return{'value':{'KDBPROMO':''},'warning':warning}
