from odoo import api,fields,models,_

class TabelAlasanTolak(models.Model):
	_name 		='ks.atolak'
	_rec_name	='KDATLK'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDATLK	 	=fields.Char(string='Kode Alasan Tolak', required = True)
	KET			=fields.Char(string='KET', required=True)

	# onchange kode alasan tolak
	@api.onchange('KDATLK')
	def onchange_KDATLK(self):
		if self.KDATLK:
			KDATLK_exist=self.env['ks.atolak'].sudo().search([
				('KDATLK','=',self.KDATLK)
				])
			if KDATLK_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Alasan Tolak sudah dipakai!'
				}
				return{'value':{'KDATLK':''},'warning':warning}
