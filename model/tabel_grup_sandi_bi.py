from odoo import api,fields,models,_



class GrupSandiBI(models.Model):
	_name ='um.grpsbi'
	_rec_name ='KDGSBI'


	KDPRS	=fields.Char(string='Kode Perusahaan', required=True)
	KDGSBI 	=fields.Char(string='Kode Grup Sandi BI', required=True)
	KET		=fields.Char(string='Keterangan', required=True)

	# Onchange Tidak Boleh Double
	@api.onchange('KDGSBI')
	def onchange_kgsb(self):
		if self.KDGSBI:
			kode_grup_sandi_bi_exist = self.env['um.grpsbi'].sudo().search([
			('KDGSBI','=',self.KDGSBI)
			])
			if kode_grup_sandi_bi_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Grup Sandi BI sudah digunakan!!!'
				}
				return{'value':{'KDGSBI':''},'warning':warning}



