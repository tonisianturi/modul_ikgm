from odoo import api,fields,models,_

class TabelKelurahan(models.Model):
	_name 		= 'um.kelurahan'
	_rec_name	= 'KDLURAH'


	KDPRS 		=fields.Char(string='Kode Perusahaan', required=True)
	KDPROP	 	=fields.Many2one(comodel_name='um.provinsi',string='Kode Provinsi')
	KDKABUD		=fields.Many2one(comodel_name='um.kabupaten',string='Kode Kabupaten')
	KDCAMAT		=fields.Many2one(comodel_name='um.kecamatan',string='Kode Kecamatan')
	KDLURAH		=fields.Char(string='Kode Kelurahan',required=True)
	NMLURAH		=fields.Char(string='Nama Kelurahan',required=True)
	KDPOS		=fields.Char(string='Kode Pos',required=True)
	

	#ONCHANGE DOMAIN
	@api.onchange('KDPROP')
	def onchange_kabupaten(self):
		self.KDKABUD = False
		#print("=====================",self.nama_provinsi)
		domain_kabupaten = {'KDKABUD': [('KDPROP', '=', self.KDPROP.id)]}
		return {'domain':domain_kabupaten}
	

	#ONCHANGE DOMAIN
	@api.onchange('KDKABUD')
	def onchange_kecamatan(self):
		self.KDCAMAT = False
		#print("=====================",self.nama_provinsi)
		domain_kecamatan = {'KDCAMAT': [('KDKABUD', '=', self.KDKABUD.id)]}
		return {'domain':domain_kecamatan}
	
	# onchange kode kelurahan
	@api.onchange('KDLURAH')
	def onchange_KDLURAH(self):
		if self.KDLURAH:
			KDLURAH_exist=self.env['um.kelurahan'].sudo().search([
				('KDLURAH','=',self.KDLURAH)
				])
			if KDLURAH_exist:
				# cek ada data atau tidak
				warning ={
					'title' : 'warning',
					'message' : 'Kode Kelurahan sudah dipakai!'
				}
				return{'value':{'KDLURAH':''},'warning':warning}
