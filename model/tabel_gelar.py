from odoo import api,fields,models,_



class TabelGelar(models.Model):
	_name ='hr.gelar'


	KDPRS		=fields.Char(string='Kode Perusahaan', required=True)
	KDGELAR	 	=fields.Char(string='Kode Gelar', required=True)
	KET 		=fields.Char(string='Keterangan', required=True)
	KDGSBI		=fields.Many2one(comodel_name='um.grpsbi',string="Kode Grup Sandi BI")
	KDSBI 		=fields.Many2one(comodel_name='um.sbi',string='Kode Sandi BI')
	
	 # Onchange Tidak Boleh Double
	@api.onchange('KDGELAR')
	def onchange_kode_gelar(self):
		if self.KDGELAR:
			kode_gelar_exist = self.env['hr.gelar'].sudo().search([
			('KDGELAR','=',self.KDGELAR)
			])
			if kode_gelar_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Gelar sudah digunakan!!!'
				}
				return{'value':{'KDGELAR':''},'warning':warning}
				

	#Onchange terisi otomatis
	@api.onchange('KDGSBI')
	def onchange_kode_grup_sandi_bi(self):
		self.KDSBI =False
		kodesandibi ={'KDSBI':[('KDGSBI','=',self.KDGSBI.id)]}
		return {'domain':kodesandibi}



