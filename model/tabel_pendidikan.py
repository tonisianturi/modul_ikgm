from odoo import api,fields,models,_



class TabelPendidikan(models.Model):
	_name ='hr.pddk'
	_rec_name ='KDPDDK'


	KDPRS		=fields.Char(string='Kode Perusahaan', required=True)
	KDPDDK	 	=fields.Char(string='Kode Pendidikan', required=True)
	KET 		=fields.Char(string='Keterangan', required=True)
	KDGSBI		= fields.Many2one(string='Kode Grup Sandi BI',comodel_name='um.grpsbi' )
	KDSBI		= fields.Many2one(string='Kode Sandi BI',comodel_name='um.sbi' )
	
	# Onchange Tidak Boleh Double
	@api.onchange('KDPDDK')
	def onchange_pendidikan(self):
		if self.KDPDDK:
			pendidikan_exist = self.env['hr.pddk'].sudo().search([
			('KDPDDK','=',self.KDPDDK)
			])
			if pendidikan_exist:
				warning = {
					'title' : 'Warning',
					'message' : 'Kode Pendidikan sudah digunakan!!!'
				}
				return{'value':{'KDPDDK':''},'warning':warning}

	#Onchange terisi otomatis
	@api.onchange('KDGSBI')
	def onchange_kode_grup_sandi_bi(self):
		self.KDSBI =False
		kodesandibi ={'KDSBI':[('KDGSBI','=',self.KDGSBI.id)]}
		return {'domain':kodesandibi}
